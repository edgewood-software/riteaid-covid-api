package com.edgewoodsoftware.covid.riteaid.api.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Builder
public class StoresNearAddressesWithAvailableSlots {
    private List<StoreModel> storesWithAvailableSlots;
    private List<String> storesChecked;
}
