package com.edgewoodsoftware.covid.riteaid.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients
@SpringBootApplication
public class RiteAidApiApplication {
	public static void main(String[] args) {
		SpringApplication.run(RiteAidApiApplication.class, args);
	}
}
