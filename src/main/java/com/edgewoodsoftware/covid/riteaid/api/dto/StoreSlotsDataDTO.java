package com.edgewoodsoftware.covid.riteaid.api.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StoreSlotsDataDTO {
    private SlotsDTO slots;
}
