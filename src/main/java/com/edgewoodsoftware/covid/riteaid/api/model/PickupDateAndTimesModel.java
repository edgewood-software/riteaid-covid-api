package com.edgewoodsoftware.covid.riteaid.api.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class PickupDateAndTimesModel {
    private List<String> regularHours;
    private SpecialHoursModel specialHours;
    private String defaultTime;
    private String earliest;
}
