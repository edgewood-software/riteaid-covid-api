package com.edgewoodsoftware.covid.riteaid.api.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class DataDTO {
    private List<StoreDTO> stores;
    private Object globalZipCode;
    private ResolvedAddressDTO resolvedAddress;
    private Object ambiguousAddresses;
    private List<Object> warnings;
}
