package com.edgewoodsoftware.covid.riteaid.api.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class PickupDateAndTimesDTO {
    private List<String> regularHours;
    private SpecialHoursDTO specialHours;
    private String defaultTime;
    private String earliest;
}
