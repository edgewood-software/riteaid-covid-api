package com.edgewoodsoftware.covid.riteaid.api.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ElementsModel {
    private Integer altitude;
    private Double latitude;
    private Double longitude;
}
