package com.edgewoodsoftware.covid.riteaid.api.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class DataModel {
    private List<StoreModel> stores;
    private Object globalZipCode;
    private ResolvedAddressModel resolvedAddress;
    private Object ambiguousAddresses;
    private List<Object> warnings;
}
