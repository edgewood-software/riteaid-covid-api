package com.edgewoodsoftware.covid.riteaid.api.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HolidayHourDTO {
    private String holidayDate;
    private String storeHours;
    private String pharmacyHours;
}
