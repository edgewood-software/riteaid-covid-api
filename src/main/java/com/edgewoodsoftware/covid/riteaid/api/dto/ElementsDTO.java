package com.edgewoodsoftware.covid.riteaid.api.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ElementsDTO {
    private Integer altitude;
    private Double latitude;
    private Double longitude;
}
