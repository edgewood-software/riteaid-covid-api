package com.edgewoodsoftware.covid.riteaid.api.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "riteaid-api")
public class RiteAidApiConfig {
    private String url;
    private List<String> addresses;
    private String covidStoresAttrFilter;
    private int covidStoresFetchMechanismVersion;
    private int delay;
    private int radius;
}
