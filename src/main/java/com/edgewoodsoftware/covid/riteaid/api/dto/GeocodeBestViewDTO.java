package com.edgewoodsoftware.covid.riteaid.api.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GeocodeBestViewDTO {
    private ElementsDTO northEastElements;
    private ElementsDTO southWestElements;
}
